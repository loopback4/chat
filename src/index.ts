import {WebsocketApplication} from "@loopback4/websocket";
import { Component, CoreBindings, inject } from '@loopback/core';
import { ChatControllerWs } from './controllers';

export class ChatComponent implements Component {
    controllers = [ChatControllerWs];

    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }
}
