import { WebsocketApplication } from "@loopback4/websocket";
import { Component } from '@loopback/core';
import { ChatControllerWs } from './controllers';
export declare class ChatComponent implements Component {
    private application;
    controllers: (typeof ChatControllerWs)[];
    constructor(application: WebsocketApplication);
}
